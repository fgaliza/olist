# Work at Olist

This project was made for applying to a position as Python Developer at OLIST. It was developed using Django and Python 3.7. It was divided in CallRecords module, where the POST requests will be made to create new call registers, and a BillRecords module responsible for storing the Pricing parameters (standing charge, minute charge and active billing hours), returning the BillRecord for a specific period and calculating each CallRecord prices.


# Environment

Notebook: 
ASUS G75V, Core I7 2.3GHz
RAM 12GB
Linux Ubuntu 16

IDE:
Sublime Text 3 with ANACONDA  
(I'm planning on climbing the gigantic mountain that is the learning curve of using VIM, but it's still a work in progress)



### How to Run and Test it
In developing, the project was run and tested using DOCKER, so It could be used as testing and running the server as follows:

Install docker and docker-compose.

Docker
https://docs.docker.com/install/

Docker-compose
https://docs.docker.com/compose/install/

The project needs a .env file on the project root, containing these the following:

```
ENVIRONMENT=DEVELOPMENT
POSTGRES_USER=root
POSTGRES_PASSWORD=pipoca123
POSTGRES_DB=pg
COMPOSE_HTTP_TIMEOUT=200
```


After installing both, on the terminal, use the command:

```
docker-compose build
```

This will build the project using its Dockerfile. Next use the command:

```
docker-compose up
```


The project was also deployed on HEROKU at this address:

https://galiza-work-at-olist.herokuapp.com/


The Django Admin page is available to alter the Pricing Parameters at this address:

https://galiza-work-at-olist.herokuapp.com/admin/

username: olist
password: workatolist


OBS: I couldn't figure out how to make Django Rest Framework documentation show the query parameters available on each endpoint. Because of this, the url https://galiza-work-at-olist.herokuapp.com/ won't work with filtering the bill records by month/year. For testing those filters, access:

https://galiza-work-at-olist.herokuapp.com/bill-records/{{PHONE NUMBER}}/?month={{MONTH}}&year{{YEAR}}


# Last Thoughts

I actually used this project as a study, in building a Django application from the ground up. I stumbled on various errors and problems I'd never been through and certainly I grew up a great deal as a developer after finishing it. A lot of things could be improved, and could've been done differently, but the main goal is to use the feedback to improve it even further.

Thanks for the opportunity!