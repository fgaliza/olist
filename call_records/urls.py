from django.urls import path, register_converter

from call_records import views
from olist import converters

register_converter(converters.PhoneConverter, 'phone')

urlpatterns = [
    path('', views.RecordList.as_view(), name='create_record'),
    path('<phone:phone>/', views.RecordDetail.as_view(),
         name='get_records')
]
