from rest_framework import serializers

from call_records.models import CallRecord


class CallRecordSerializer(serializers.Serializer):
    call_id = serializers.IntegerField(required=True)
    call_start = serializers.DateTimeField()
    call_end = serializers.DateTimeField()
    source = serializers.CharField(
        max_length=11, min_length=10, allow_blank=True)
    destination = serializers.CharField(
        max_length=11, min_length=10, allow_blank=True)


class StartRecordSerializer(serializers.ModelSerializer):

    class Meta:
        model = CallRecord
        fields = ('id', 'call_id', 'type',
                  'timestamp', 'source', 'destination')


class EndRecordSerializer(serializers.ModelSerializer):

    class Meta:
        model = CallRecord
        fields = ('id', 'call_id', 'type', 'timestamp')


class CallRecordPostSerializer(serializers.Serializer):

    call_id = serializers.IntegerField(
        required=True,
        help_text='Unique for each call record pair')
    type = serializers.CharField(
        max_length=5, min_length=3, required=True,
        help_text="Indicate if it's 'start' or 'end' record"
    )
    timestamp = serializers.IntegerField(
        required=True,
        help_text='The timestamp of when the event occured')
    source = serializers.CharField(
        max_length=11, min_length=10, required=False,
        help_text='The subscriber phone number that originated the call. Required when type is "start"'
    )
    destination = serializers.CharField(
        max_length=11, min_length=10, required=False,
        help_text='The phone number receiving the call. Required when type is "start"'
    )
