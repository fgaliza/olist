# Generated by Django 2.1 on 2018-08-17 18:52

from django.db import migrations, models
import olist.validators


class Migration(migrations.Migration):

    dependencies = [
        ('call_records', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='callrecord',
            old_name='origin',
            new_name='source',
        ),
        migrations.AlterField(
            model_name='callrecord',
            name='call_id',
            field=models.IntegerField(help_text='Unique for each call record pair'),
        ),
        migrations.AlterField(
            model_name='callrecord',
            name='timestamp',
            field=models.IntegerField(help_text='The timestamp of when the event occured', validators=[olist.validators.validate_future_date]),
        ),
        migrations.AlterField(
            model_name='callrecord',
            name='type',
            field=models.CharField(choices=[('start', 'start'), ('end', 'end')], help_text="Indicate if it's 'start' or 'end' record", max_length=5),
        ),
    ]
