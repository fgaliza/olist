from django.db import models

from call_records.utils import ChoiceEnum
from olist.validators import validate_phone_min_length, validate_future_date


class CallRecord(models.Model):

    class RecordChoices(ChoiceEnum):
        start = 'start'
        end = 'end'

    call_id = models.IntegerField(
        null=False, blank=False, help_text='Unique for each call record pair')
    type = models.CharField(
        max_length=5, choices=RecordChoices.choices(), help_text="Indicate if it's 'start' or 'end' record")
    timestamp = models.IntegerField(validators=[
                                    validate_future_date], help_text='The timestamp of when the event occured')
    source = models.CharField(
        max_length=11,
        validators=[validate_phone_min_length],
        help_text='The subscriber phone number that originated the call. Required when type is "start"'
    )
    destination = models.CharField(
        max_length=11,
        validators=[validate_phone_min_length],
        help_text='The phone number receiving the call. Required when type is "start"'
    )

    def __init__(self, *args, **kwargs):
        super(CallRecord, self).__init__(*args, **kwargs)
        if self.type:
            class_map = {
                "start": StartRecord,
                "end": EndRecord
            }
            self.__class__ = class_map.get(self.type, CallRecord)

    class Meta:
        db_table = 'call_records'
        app_label = 'call_records'
        verbose_name = "CallRecord"
        verbose_name_plural = "CallRecords"
        unique_together = ('call_id', 'type')


class StartRecord(CallRecord):

    class Meta:
        proxy = True


class EndRecord(CallRecord):

    class Meta:
        proxy = True
