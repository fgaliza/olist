from django.apps import AppConfig


class CallRecordsConfig(AppConfig):
    name = 'call_records'

    def ready(self):
        import call_records.signals  # noqa
