from datetime import datetime

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from call_records.serializers import (
    StartRecordSerializer, EndRecordSerializer,
    CallRecordSerializer, CallRecordPostSerializer
)
from call_records.models import CallRecord


class RecordList(APIView):
    """
    Create Call Records.
    """

    def post(self, request, format=None):
        if request.data.get('type') == 'start':
            serializer = StartRecordSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        elif request.data.get('type') == 'end':
            serializer = EndRecordSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_serializer(self):
        return CallRecordPostSerializer()


class RecordDetail(APIView):
    """
    Get Call Records from a specific phone
    """

    def get(self, request, phone, format=None):
        records = CallRecord.objects.filter(source=phone)
        call_record_list = []
        for start_record in records:
            end_record = CallRecord.objects.get(
                call_id=start_record.call_id, type='end')
            if end_record:
                call_record_dict = {
                    'call_id': start_record.call_id,
                    'call_start': datetime.fromtimestamp(start_record.timestamp).strftime("%Y-%m-%d %H:%M:%S"),
                    'call_end': datetime.fromtimestamp(end_record.timestamp).strftime("%Y-%m-%d %H:%M:%S"),
                    'source': start_record.source,
                    'destination': start_record.destination,
                }
                call_record_list.append(call_record_dict)
        if call_record_list:
            serializer = CallRecordSerializer(call_record_list, many=True)
            return Response(serializer.data, status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)
