from datetime import datetime
import json

from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status

from bill_records.models import Pricing
from call_records.models import CallRecord, StartRecord, EndRecord
from call_records.serializers import CallRecordSerializer

client = Client()


class TestCallRecordView(TestCase):

    def setUp(self):
        self.source_phone = '99988526423'
        self.destionation_phone = '9993468278'
        Pricing.objects.create(
            standing_charge=0.36,
            minute_charge=0.09,
            billing_start='06:00:00',
            billing_end='22:00:00',
        )
        self.valid_start = {
            "call_id": 2,
            "type": 'start',
            "timestamp": int(datetime.now().timestamp()),
            "source": self.source_phone,
            "destination": self.destionation_phone,
        }
        self.invalid_start = {
            "type": 'end',
            "timestamp": int(datetime.now().timestamp()),
            "source": self.source_phone,
            "destination": self.destionation_phone,
        }
        self.valid_end = {
            "call_id": 2,
            "type": 'end',
            "timestamp": int(datetime.now().timestamp()),
        }
        self.invalid_end = {
            "call_id": 2,
            "type": 'end',
            "timestamp": int(datetime.now().timestamp()),
            "source": self.source_phone,
            "destination": self.destionation_phone,
        }

    def test_post_valid_start_record(self):
        response = client.post(reverse('create_record'), data=json.dumps(
            self.valid_start), content_type='application/json')
        db_response = CallRecord.objects.get(
            call_id=self.valid_start.get('call_id'),
            type=self.valid_start.get('type'))
        self.assertEqual(response.data.get('call_id'), db_response.call_id)
        self.assertEqual(response.data.get('type'), db_response.type)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsInstance(db_response, StartRecord)

    def test_post_valid_end_record(self):
        response = client.post(reverse('create_record'), data=json.dumps(
            self.valid_end), content_type='application/json')
        db_response = CallRecord.objects.get(
            call_id=self.valid_end.get('call_id'),
            type=self.valid_end.get('type'))
        self.assertEqual(response.data.get('call_id'), db_response.call_id)
        self.assertEqual(response.data.get('type'), db_response.type)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsInstance(db_response, EndRecord)

    def test_post_invalid_start_record(self):
        response = client.post(reverse('create_record'), data=json.dumps(
            self.invalid_start), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_invalid_end_record(self):
        response = client.post(reverse('create_record'), data=json.dumps(
            self.invalid_start), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_duplicate_start_record(self):
        start_response = client.post(reverse('create_record'), data=json.dumps(
            self.valid_start), content_type='application/json')

        start_error_response = client.post(reverse('create_record'), data=json.dumps(
            self.valid_start), content_type='application/json')

        self.assertEqual(start_response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(start_error_response.status_code,
                         status.HTTP_400_BAD_REQUEST)

    def test_duplicate_end_record(self):
        end_response = client.post(reverse('create_record'), data=json.dumps(
            self.valid_end), content_type='application/json')

        end_error_response = client.post(reverse('create_record'), data=json.dumps(
            self.valid_end), content_type='application/json')

        self.assertEqual(end_response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(end_error_response.status_code,
                         status.HTTP_400_BAD_REQUEST)

    def test_get_call_record(self):
        CallRecord.objects.create(
            call_id=19,
            type='start',
            source=self.source_phone,
            destination=self.destionation_phone,
            timestamp=int(datetime.now().timestamp())
        )
        CallRecord.objects.create(
            call_id=19, type='end', timestamp=int(datetime.now().timestamp()))
        response = client.get(
            reverse('get_records', kwargs={'phone': self.source_phone}))
        serializer = self.get_call_records_from_phone(self.source_phone)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def get_call_records_from_phone(self, phone):
        records = CallRecord.objects.filter(source=phone)
        call_record_list = []
        for start_record in records:
            end_record = CallRecord.objects.get(
                call_id=start_record.call_id, type='end')
            if end_record:
                call_record_dict = {
                    'call_id': start_record.call_id,
                    'call_start': datetime.fromtimestamp(start_record.timestamp).strftime("%Y-%m-%d %H:%M:%S"),
                    'call_end': datetime.fromtimestamp(end_record.timestamp).strftime("%Y-%m-%d %H:%M:%S"),
                    'source': start_record.source,
                    'destination': start_record.destination,
                }
                call_record_list.append(call_record_dict)
        return CallRecordSerializer(call_record_list, many=True)
