from datetime import datetime

from django.test import TestCase
from django.db.utils import IntegrityError

from call_records.models import CallRecord, StartRecord, EndRecord


class TestCallRecordModel(TestCase):

    def setUp(self):
        self.start_record = CallRecord(
            call_id=1,
            type='start',
            timestamp=datetime.now().timestamp(),
            source='99988526423',
            destination='9993468278',
        )

        self.end_record = CallRecord(
            call_id=1,
            type='end',
            timestamp=datetime.now().timestamp(),
        )

    def test_model_creation(self):
        self.start_record.save()
        self.assertEqual(self.start_record.call_id, 1)

    def test_start_proxy(self):
        self.start_record.save()
        self.assertIsInstance(self.start_record, StartRecord)

    def test_end_proxy(self):
        self.end_record.save()
        self.assertIsInstance(self.end_record, EndRecord)

    def test_duplicate_start_record(self):
        self.start_record.save()
        self.assertRaises(IntegrityError, self.start_record.save())

    def test_duplicate_end_record(self):
        self.end_record.save()
        self.assertRaises(IntegrityError, self.end_record.save())
