from datetime import datetime

from django.db.models.signals import post_save
from django.dispatch import receiver

from bill_records.models import BillRecord, Pricing
from bill_records.services import calc_call_price, calc_billable_minutes
from call_records.models import StartRecord, EndRecord, CallRecord


@receiver(post_save, sender=StartRecord)
def start_record_bill_validation(sender, instance, created, **kwargs):
    if created:
        record = CallRecord.objects.filter(
            call_id=instance.call_id, type='end').first()
        if record:
            create_bill_record(start=instance, end=record)


@receiver(post_save, sender=EndRecord)
def end_record_bill_validation(sender, instance, created, **kwargs):
    if created:
        record = CallRecord.objects.filter(
            call_id=instance.call_id, type='start').first()
        if record:
            create_bill_record(start=record, end=instance)


def create_bill_record(start, end):
    start_datetime = datetime.fromtimestamp(start.timestamp)
    end_datetime = datetime.fromtimestamp(end.timestamp)
    duration = start_datetime - end_datetime
    pricing = Pricing.objects.last()
    minutes = calc_billable_minutes(start_datetime, end_datetime, pricing)
    price = calc_call_price(minutes, pricing)
    BillRecord.objects.create(
        source=start.source,
        destination=start.destination,
        call_start_date=start_datetime.date(),
        call_end_date=start_datetime.date(),
        call_start_time=start_datetime.time(),
        duration=duration,
        price=price
    )
