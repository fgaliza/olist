from datetime import datetime

from django.core.exceptions import ValidationError


def validate_phone_min_length(phone):
    if len(phone) < 10:
        raise ValidationError(
            'Invalid Phone. Value must be at least 10 characters')


def validate_future_date(timestamp):
    date = datetime.fromtimestamp(timestamp)
    if date > datetime.now():
        raise ValidationError(
            'Invalid Date. Not possible to registar future dates')
