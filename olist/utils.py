from datetime import timedelta


def get_previous_month(current):
    _first_day = current.replace(day=1)
    prev_month_lastday = _first_day - timedelta(days=1)
    prev_month = prev_month_lastday.replace(day=1)
    return prev_month.month
