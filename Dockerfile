FROM python:3.7-rc

ENV PYTHONBUFFERED 1

RUN pip install pipenv

RUN mkdir -p /app

ADD . /app
WORKDIR /app

COPY Pipfile /app

RUN pipenv install --deploy --system --verbose