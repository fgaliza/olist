from django.db import models

from olist.validators import validate_phone_min_length


class BillRecord(models.Model):

    source = models.CharField(
        max_length=11,
        null=False,
        blank=False,
        validators=[validate_phone_min_length]
    )

    destination = models.CharField(
        max_length=11,
        null=False,
        blank=False,
        validators=[validate_phone_min_length]
    )
    call_start_date = models.DateField(null=False, blank=False)
    call_end_date = models.DateField(null=False, blank=False)
    call_start_time = models.TimeField(null=False, blank=False)
    duration = models.DurationField(null=False, blank=False)
    price = models.DecimalField(
        decimal_places=2, max_digits=10, null=False, blank=False)

    class Meta:
        db_table = 'bill_records'
        app_label = 'bill_records'
        verbose_name = "BillRecord"
        verbose_name_plural = "BillRecords"


class Pricing(models.Model):

    standing_charge = models.DecimalField(
        decimal_places=2, max_digits=10, null=False, blank=False)
    minute_charge = models.DecimalField(
        decimal_places=2, max_digits=10, null=False, blank=False)
    billing_start = models.TimeField()
    billing_end = models.TimeField()
