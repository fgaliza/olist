from datetime import datetime


def calc_billable_minutes(call_start, call_end, pricing):
    
    datetime_billing_start = datetime.combine(today.date(), pricing.billing_start)
    datetime_billing_end = datetime.combine(today.date(), pricing.billing_end)
    
    call_duration = call_end - call_start

    validate_start = datetime_billing_start.time() < call_start.time() < datetime_billing_end.time()
    validate_end = datetime_billing_start.time() < call_end.time() < datetime_billing_end.time()
    
    if validate_start and validate_end:
        delta = call_end - call_start
        minutes = (delta.seconds) // 60
    
    elif not validate_start and not validate_end:
        minutes = 0
    else:
        today = datetime.today()
        if call_start.time() < datetime_billing_start.time():
            start_delta = datetime_billing_start
        else:
            if call_duration.days == 0 and call_start.time() > datetime_billing_end.time():
                start_delta = datetime_billing_start
            else:
                start_delta = call_start
        if call_end.time() > datetime_billing_end.time():
            end_delta = datetime_billing_end
        else:
            if call_duration.days == 0 and call_end.time() < datetime_billing_start.time():
                end_delta = datetime_billing_start
            else:
                end_delta = call_end

        hours = end_delta - start_delta
        if hours.days != 0:
            hours = start_delta - end_delta
        
        minutes = (hours.seconds) // 60
    
    billable_minutes = datetime_billing_end - datetime_billing_start
    total_minutes = minutes + (call_duration.days * (billable_minutes.seconds//60))

    return total_minutes

def calc_call_price(minutes, pricing):
    total = (minutes * pricing.minute_charge) + pricing.standing_charge
    return total
