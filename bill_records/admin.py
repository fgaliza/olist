from django.contrib import admin

from bill_records.models import Pricing


@admin.register(Pricing)
class PricingAdmin(admin.ModelAdmin):

    fields = (
        'standing_charge',
        'minute_charge',
        'billing_start',
        'billing_end',
    )
