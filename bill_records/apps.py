from django.apps import AppConfig


class BillRecordsConfig(AppConfig):
    name = 'bill_records'
