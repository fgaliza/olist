from datetime import datetime, timedelta

from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status

from bill_records.models import BillRecord
from bill_records.serializers import BillRecordSerializer
from olist.utils import get_previous_month

client = Client()


class TestBillRecordView(TestCase):

    def setUp(self):
        self.call_start_date = datetime.now().date()
        self.call_end_date = datetime.now().date()
        self.call_start_time = datetime.now().time()
        self.source_phone = '61995803102'
        self.destionation_phone = '41995308288'
        self.valid_bill_record = BillRecord(
            source=self.source_phone,
            destination=self.destionation_phone,
            call_start_date=self.call_start_date,
            call_end_date=self.call_end_date,
            call_start_time=self.call_start_time,
            duration=timedelta(days=20, hours=10),
            price=9.5
        )

    def test_get_bill_record(self):

        BillRecord.objects.create(
            source=self.source_phone,
            destination=self.destionation_phone,
            call_start_date=self.call_start_date,
            call_end_date=self.call_end_date,
            call_start_time=self.call_start_time,
            duration=timedelta(days=20, hours=10),
            price=9.5
        )

        response = client.get(
            reverse('get_bill', kwargs={'phone': self.source_phone}))
        serializer = self.get_bill_records_from_phone(
            phone=self.source_phone,
            month=get_previous_month(self.call_end_date),
            year=self.call_end_date.year
        )

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_filtered_bill_record(self):
        self.valid_bill_record.save()
        url = reverse('get_bill', kwargs={
                      'phone': self.source_phone}) + '?month=7&year=2018'
        response = client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for bill in response.data:
            split_date = bill.get('call_start_date').split('-')
            month = split_date[1]
            year = split_date[0]
            self.assertEqual(int(month), 7)
            self.assertEqual(int(year), 2018)

    def get_bill_records_from_phone(self, phone, month, year):
        records = BillRecord.objects.filter(
            source=phone, call_end_date__month=month, call_end_date__year=year)
        return BillRecordSerializer(records, many=True)
