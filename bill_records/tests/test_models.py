from datetime import datetime

from django.test import TestCase

from bill_records.models import BillRecord, Pricing


class TestBillRecordModel(TestCase):

    def test_model_creation(self):
        record = BillRecord(
            source='41999999999',
            destination='41998569856',
            call_start_date=datetime.now().date(),
            call_end_date=datetime.now().date(),
            call_start_time=datetime.now().time(),
            duration=datetime.now().time(),
            price=9.5)
        record.save()
        self.assertEqual(record.destination, '41998569856')


class TestPricingModel(TestCase):

    def test_model_creation(self):
        pricing = Pricing(
            standing_charge=0.36,
            minute_charge=0.09,
            billing_start='06:00:00',
            billing_end='22:00:00',
        )
        pricing.save()
        self.assertEqual(pricing.standing_charge, 0.36)
