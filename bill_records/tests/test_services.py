from datetime import datetime
from decimal import Decimal
from django.test import TestCase

from bill_records.services import calc_billable_minutes, calc_call_price
from bill_records.models import Pricing


class TestBillingServiceModel(TestCase):

    def setUp(self):
        Pricing.objects.create(
            standing_charge=0.36,
            minute_charge=0.09,
            billing_start='06:00:00',
            billing_end='22:00:00',
        )
        self.pricing = Pricing.objects.last()

    def test_calc_billable_minutes(self):

        call_start = datetime(
            2018, 3, 2, 7, 0, 0, 0)
        call_end = datetime(
            2018, 3, 2, 8, 0, 0, 0)
        minutes = calc_billable_minutes(
            call_start,
            call_end,
            self.pricing
        )
        self.assertEqual(minutes, 60)

        price = calc_call_price(minutes, self.pricing)
        self.assertEqual(price, Decimal('5.76'))

        call_start = datetime(
            2018, 3, 2, 5, 0, 0, 0)
        call_end = datetime(
            2018, 3, 2, 6, 0, 0, 0)
        minutes = calc_billable_minutes(
            call_start,
            call_end,
            self.pricing
        )
        self.assertEqual(minutes, 0)
        price = calc_call_price(minutes, self.pricing)
        self.assertEqual(price, Decimal('0.36'))

        call_start = datetime(
            2018, 3, 2, 21, 0, 0, 0)
        call_end = datetime(
            2018, 3, 2, 23, 0, 0, 0)
        minutes = calc_billable_minutes(
            call_start,
            call_end,
            self.pricing
        )
        self.assertEqual(minutes, 60)
        price = calc_call_price(minutes, self.pricing)
        self.assertEqual(price, Decimal('5.76'))

        call_start = datetime(
            2018, 3, 2, 6, 0, 0, 0)
        call_end = datetime(
            2018, 3, 3, 6, 0, 0, 0)
        minutes = calc_billable_minutes(
            call_start,
            call_end,
            self.pricing
        )
        self.assertEqual(minutes, 960)
        price = calc_call_price(minutes, self.pricing)
        self.assertEqual(price, Decimal('86.76'))
