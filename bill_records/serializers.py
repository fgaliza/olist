from rest_framework import serializers

from bill_records.models import BillRecord


class BillRecordSerializer(serializers.ModelSerializer):

    class Meta:
        model = BillRecord
        fields = ('id', 'destination', 'call_start_date',
                  'call_start_time', 'duration', 'price')
