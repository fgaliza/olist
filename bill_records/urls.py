from django.urls import path, register_converter

from bill_records import views
from olist import converters

register_converter(converters.PhoneConverter, 'phone')

urlpatterns = [
    path("<phone:phone>/", views.BillDetail.as_view(), name='get_bill')
]
