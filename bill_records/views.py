from datetime import datetime

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from bill_records.serializers import BillRecordSerializer
from bill_records.models import BillRecord
from olist.utils import get_previous_month


class BillDetail(APIView):
    """
    Get Bill from specific phone

    """

    def get(self, request, phone, format=None):
        today = datetime.today()
        month = request.GET.get('month', get_previous_month(today))
        year = request.GET.get('year', today.year)
        records = BillRecord.objects.filter(source=phone)
        if records:
            if int(year) <= today.year:
                records = records.filter(call_end_date__year=int(year))
            else:
                return Response(status.HTTP_404_NOT_FOUND)
            if int(month) < today.month:
                records = records.filter(call_end_date__month=int(month))
            else:
                return Response(status.HTTP_404_NOT_FOUND)
            serializer = BillRecordSerializer(records, many=True)
            return Response(serializer.data, status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)
